# Android Talk Slides

It's just a boring latex doc. Run:

```bash
pdflatex presentation.tex
```

As with every good latex doc. You'll probably need to compile it twice if you
want nav to show up properly. So again...

```bash
pdflatex presentation.tex
```

